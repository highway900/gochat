package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"log"
	"time"
)

type PGNotifyMessage struct {
}

type PGMessage struct {
}

type PGDB struct {
	user     string
	password string
	database string
	port     string
	db       *sql.DB
	listener *pq.Listener
}

func MakePGDB(user, password, database string) *PGDB {
	db := &PGDB{}
	db.user = user
	db.password = password
	db.database = database
	return db
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func (pg *PGDB) MakeConnectionString() string {
	// This string caused major issues.
	// using string parameter=arg simply did NOT work.
	log.Println("WARNING: sslmode disabled!")
	return fmt.Sprintf(
		"postgres://%s:%s@localhost/%s?sslmode=disable",
		pg.user, pg.password, pg.database)
}

func (pg *PGDB) Connect() error {
	conninfo := pg.MakeConnectionString()

	db, err := sql.Open("postgres", conninfo)
	if err != nil {
		return err
	}

	pg.db = db

	// Setup the PG LISTEN/NOTIFY connection
	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	pg.listener = pq.NewListener(conninfo, 10*time.Second, time.Minute, reportProblem)
	err = pg.listener.Listen("events")
	checkErr(err)

	return nil
}

func (pg *PGDB) Disconnect() error {
	err := pg.db.Close()
	return err
}

func (pg *PGDB) Read(messageCount int) []*Message {
	rows, err := pg.db.Query("SELECT msg FROM context WHERE msg ->> 'message' != '' ORDER BY id DESC LIMIT $1;", messageCount)
	checkErr(err)
	defer rows.Close()
	messageArray := make([]*Message, messageCount)
	i := messageCount-1
	for rows.Next() {
		var m []byte
		err = rows.Scan(&m)
		checkErr(err)
		messageArray[i], _ = ParseMessageJSON(m)
		i--
	}
	log.Println(messageArray)
	return messageArray
}

func (pg *PGDB) Write(message Message) error {
	var lastInsertId int
	err := pg.db.QueryRow("INSERT INTO context(msg) VALUES($1) returning id;", message.MessagetoJSONString()).Scan(&lastInsertId)
	checkErr(err)
	log.Println("Last insert ID:", lastInsertId, message)
	return err
}

// Needs to be called from a goroutine
func (pg *PGDB) Listen(notifyChannel chan *Message) error {
	log.Println("Listening...")

	type D struct {
		Id  int      `json:"id"`
		Msg *Message `json:"msg"`
	}

	type V struct {
		Table  string `json:"table"`
		Action string `json:"action"`
		Data   D      `json:"data"`
	}

	var v V
	for {
		notify := <-pg.listener.Notify

		json.Unmarshal([]byte(notify.Extra), &v)

		notifyChannel <- v.Data.Msg
	}
	return nil
}
