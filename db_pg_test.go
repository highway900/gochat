package main

import (
	"fmt"
	"log"
	"sync"
	"testing"
)

func CleanupWrites(d *PGDB) {
	// Cleanup after insert
	_, err := d.db.Query("DELETE FROM context WHERE id > 0")
	checkErr(err)
}

func Setup() *PGDB {
	d := &PGDB{}

	d.database = "test_context"
	d.user = ""
	d.password = ""

	err := d.Connect()
	if err != nil {
		fmt.Println("Connection to DB failed")
	}

	return d
}

func TestListen(t *testing.T) {
	d := Setup()
	defer d.db.Close()
	defer CleanupWrites(d)

	// We have to use a sync.waitGroup to make sure the channel result
	// is processed before the test completes.
	var wg sync.WaitGroup
	wg.Add(1)

	// Delcare this here so we can compare with returned result
	m := Message{}
	m.Message = "test listen"
	m.Channel = "test pattern"

	broadcast := make(chan *Message)

	// Broadcast functionality
	go func() {
		for {
			log.Println("waiting for <-broadcast")
			select {
			case newMessage := <-broadcast:
				log.Println("<-broadcast:", newMessage)
				if newMessage.Message == m.Message &&
					newMessage.Channel == m.Channel {
					//
					log.Println("Listen Test Success")
				} else {
					log.Println("Listen Test Failed")
					t.Fail()
				}

				defer wg.Done()
				return
			}
		}
	}()

	go d.Listen(broadcast)

	err := d.Write(m)
	if err != nil {
		log.Println("Write to DB failed")
	}

	// Cleanup after insert
	_, err = d.db.Query("DELETE FROM context WHERE id > 0")
	checkErr(err)
	wg.Wait()
}

func TestRead(t *testing.T) {
	d := Setup()
	defer d.db.Close()
	defer CleanupWrites(d)


	c := 5

	for i:=0; i<c; i++ {
		m := Message{}
		m.Message = fmt.Sprintf("test insert %d", i)

		err := d.Write(m)
		if err != nil {
			fmt.Println("Write to DB failed")
		}
	}

	d.Read(c)
}

func TestInsert(t *testing.T) {
	d := Setup()
	defer d.db.Close()
	defer CleanupWrites(d)

	m := Message{}
	m.Message = "test insert"

	err := d.Write(m)
	if err != nil {
		fmt.Println("Write to DB failed")
	}
}

func TestConnect(t *testing.T) {
	d := Setup()
	defer d.db.Close()
}
