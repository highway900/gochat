package main

import "log"

type Channel struct {
	id      string
	clients []*Client
}

func (c *Channel) RegisterClient(client *Client) {
	c.clients = append(c.clients, client)
}

func (c *Channel) ClientInChannel(a *Client) bool {
	for _, b := range c.clients {
		if b == a {
			return true
		}
	}
	return false
}

// hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Inbound messages from the clients.
	broadcast chan *Message

	// channels
	channels map[string]*Channel

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// Registered clients.
	clients map[*Client]bool

	// Storage Interface for persistence
	db IStore
}

func newHub() *Hub {
	return &Hub{
		broadcast:  make(chan *Message),
		channels:   make(map[string]*Channel),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func (h *Hub) GetOrMakeChannel(id string) (*Channel, bool) {
	channel := h.channels[id]
	if channel == nil {
		channel = &Channel{}
		channel.id = id
		h.channels[id] = channel
		log.Println("DEBUG: Making Channel:", channel)
		return channel, true
	}
	log.Println("DEBUG: Using Channel:", channel)
	return channel, false
}

func (h *Hub) run() {
	go h.db.Listen(h.broadcast)
	for {
		select {
		case client := <-h.register:
			log.Println("DEBUG: hub registering client", client)
			h.clients[client] = true
		case client := <-h.unregister:
			log.Println("DEBUG: hub unregistering client", client)
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			log.Printf("DEBUG: hub Broadcasting to: %d clients @%s",
				len(h.channels[message.Channel].clients), h.channels[message.Channel].id)

			for i, client := range h.channels[message.Channel].clients {

				// Cleanup dead clients in channel
				if !h.clients[client] {
					h.channels[message.Channel].clients = append(
						h.channels[message.Channel].clients[:i], h.channels[message.Channel].clients[i+1:]...)
					log.Println("should be 0", len(h.channels[message.Channel].clients))
					continue
				}

				select {
				case client.send <- message.MessagetoJSONByteArray():
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		
		}
	}
}
