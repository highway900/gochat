package main

import (
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"text/template"
)

var addr = flag.String("addr", ":8080", "http service address")

type manager struct {
	hub *Hub
}

func (m *manager) serveHome(w http.ResponseWriter, r *http.Request) {
	var homeTemplate = template.Must(template.ParseFiles("serve/home.html"))
	vars := mux.Vars(r)

	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}

	m.hub.GetOrMakeChannel(vars["channel"])

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	homeTemplate.Execute(w, r.Host)
}

// serveWs handles websocket requests from the peer.
func (m *manager) serveWs(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: m.hub, conn: conn, send: make(chan []byte, 256)}
	m.hub.register <- client
	go client.writePump()
	client.readPump()
}

func main() {
	flag.Parse()

	_manager := &manager{
		newHub(),
	}

	// Connect to the DB
	_manager.hub.db = MakePGDB("", "", "test_context")
	_manager.hub.db.Connect()

	r := mux.NewRouter()

	go _manager.hub.run()
	r.HandleFunc("/", _manager.serveHome)
	r.HandleFunc("/{channel}/", _manager.serveHome)
	r.HandleFunc("/ws", _manager.serveWs)
	r.HandleFunc("/ws/", _manager.serveWs)

	err := http.ListenAndServe(*addr, r)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
