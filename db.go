package main

type IStore interface {
	Connect() error
	Disconnect() error
	Write(message Message) error
	Read(messageCount int) []*Message
	Listen(chan *Message) error
}
