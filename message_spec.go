package main

import (
	"encoding/json"
	"errors"
	"fmt"
)

type Message struct {
	Message string `json:"message"`
	Channel string `json:"channel"`
	Client  *Client
}

func ParseMessageJSON(in []byte) (*Message, error) {
	msg := &Message{}
	json.Unmarshal(in, msg)
	if len(msg.Channel) == 0 {
		return msg, errors.New("No channel provided")
	}
	return msg, nil
}

func (msg *Message) MessagetoJSONByteArray() []byte {
	m, err := json.Marshal(msg)
	if err != nil {
		errors.New("Failed to parse Message to JSON byte array")
	}
	return []byte(m)
}

func (msg *Message) MessagetoJSONString() string {
	m, err := json.Marshal(msg)
	if err != nil {
		errors.New("Failed to parse Message to JSON string")
	}
	return string(m)
}

func (msg *Message) String() string {
	return fmt.Sprintf("MESSAGE@%s: [%s]", msg.Channel, msg.Message)
}
